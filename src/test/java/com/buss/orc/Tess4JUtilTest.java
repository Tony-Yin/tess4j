package com.buss.orc;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.buss.ocr.Tess4JUtil;

import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

public class Tess4JUtilTest {

	@Test
	public void testTess4JUtil() {
		new Tess4JUtil();
	}

	@Test
	public void testUrl() throws IOException, TesseractException {
		assertEquals("撩", Tess4JUtil.recogURL("http://wap.qimiaoxs.com/image/liao.jpg"));
	}

	@Test
	public void testFile() throws IOException, TesseractException {
		assertEquals("黑日 日法苏",
				Tess4JUtil.recogFile(new File(LoadLibs.extractTessResources("tessdata").getAbsolutePath(), "ocr.png")));
	}

}