package com.buss.ocr;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

public class Tess4JUtil {
	private static final ITesseract instance = new Tesseract();
	static {
		instance.setDatapath(LoadLibs.extractTessResources("tessdata").getAbsolutePath());
		instance.setLanguage("chi_sim");
	}

	public static String recogURL(URL url) throws IOException, TesseractException {
		try (InputStream is = url.openStream()) {
			return recogInputStream(is);
		}
	}

	public static String recogURL(String url) throws IOException, TesseractException {
		return recogURL(new URL(url));
	}

	public static String recogInputStream(InputStream is) throws IOException, TesseractException {
		return recogImage(ImageIO.read(is));
	}

	public static String recogFile(File file) throws IOException, TesseractException {
		return instance.doOCR(file).trim();
	}

	public static String recogImage(BufferedImage bi) throws IOException, TesseractException {
		return instance.doOCR(bi).trim();
	}

}
